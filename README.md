# Filebeat #

This README contains the README provided in the filebeat download. Additional deployment  commands below:

### What is this repository for? ###
* Deploying filebeat as a windows service to forward logs into logstash

### Get started ###

* Edit deploymentscripts/environments.ps1 to configure a list of environments with key/value config settings eg:

```powershell
$environments = @{
        dev = @{
            logFileGlob = 'C:\logs\*';
            logstashHosts = '["127.0.0.1:5044"]';
            deploymentDirectory = 'C:\\testDeployments\\filebeat';
            }; 
        qa = @{
            logFileGlob = 'C:\logs\*';
            logstashHosts = '["10.10.101.111:5044"]';
            deploymentDirectory = 'C:\\testQaDeployments\\filebeat';
            };
        live = @{
            logFileGlob = 'C:\logs\*';
            logstashHosts = '["10.10.101.123:5044"]';
            deploymentDirectory = 'C:\\testLiveDeployments\\filebeat';   
            };
    }   
```

* run deploymentscripts/install.bat $env$ to deploy to an environment (if env not provided defauts to dev)

```install.bat qa```

### Troubleshooting ###

* If logs are not arriving at elastic search check your log file glob in filebeat.yml in the deployment directory contains logs which have been written to in the last 24 hours.

* You can run a quick check to see if logstash is available for an environment (run on deployment target). This will test for logstash on port 5044 (beats protocol default)

``` powershell -command ". .\install.ps1; CheckLogstash -env dev ```


----------


# Original ReadMe from filebeat #

## Welcome to filebeat 5.2.2 ##

Filebeat sends log files to Logstash or directly to Elasticsearch.

## Getting Started

To get started with filebeat, you need to set up Elasticsearch on your localhost first. After that, start filebeat with:

    ./filebeat  -c filebeat.yml -e

This will start the beat and send the data to your Elasticsearch instance. To load the dashboards for filebeat into Kibana, run:

    ./scripts/import_dashboards

For further steps visit the [Getting started](https://www.elastic.co/guide/en/beats/filebeat/5.2/filebeat-getting-started.html) guide.

## Documentation

Visit [Elastic.co Docs](https://www.elastic.co/guide/en/beats/filebeat/5.2/index.html) for the full filebeat documentation.

## Release notes

https://www.elastic.co/guide/en/beats/libbeat/5.2/release-notes-5.2.2.html
