@echo off


IF "%~1"=="" goto setDefaultEnvironment
IF NOT "%~1"=="" goto setEnvironmentFromCommandLine


:setDefaultEnvironment
SET FILEBEAT_ENV=dev
goto transformConfigs

:setEnvironmentFromCommandLine
SET FILEBEAT_ENV="%~1"
goto transformConfigs


:transformConfigs

powershell -command ". .\install.ps1; InstallFilebeat -env %FILEBEAT_ENV%; exit $LASTEXITCODE"

