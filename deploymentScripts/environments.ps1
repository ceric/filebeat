$environments = @{
        dev = @{
            logFileGlob = 'C:\logs\*';
            logstashHosts = '["127.0.0.1:5044"]';
            deploymentDirectory = 'C:\\testDeployments\\filebeat';
            }; 
        qa = @{
            logFileGlob = 'C:\logs\*';
            logstashHosts = '["*****:5044"]';
            deploymentDirectory = 'C:\\testQaDeployments\\filebeat';
            };
        live = @{
            logFileGlob = 'C:\logs\*';
            logstashHosts = '["*******:5044"]';
            deploymentDirectory = 'C:\\testLiveDeployments\\filebeat';   
            };
    }   