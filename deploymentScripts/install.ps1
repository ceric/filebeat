. .\testPort.ps1
. .\environments.ps1

function CopyToinstallationDirectory
{
    param([string]$deploymentDirectory)

    If (Test-Path $deploymentDirectory){
        Remove-Item $deploymentDirectory -recurse
    }

    New-Item -ItemType directory -Path $deploymentDirectory

    Copy-Item -Path "$PSScriptRoot\..\install-service-filebeat.ps1" -Destination $deploymentDirectory
    Copy-Item -Path "$PSScriptRoot\..\filebeat.exe" -Destination  $deploymentDirectory
    Copy-Item -Path "$PSScriptRoot\..\filebeat.yml" -Destination  $deploymentDirectory
}

function ReplaceConfig
{
    param($env, $config)

    $deploymentDirectory = $config.deploymentDirectory

    (Get-Content "$deploymentDirectory\filebeat.yml").replace(
        '${logFileGlob}',$config.logFileGlob).replace(
        '${env}',$env).replace(
        '${logstashHosts}',$config.logstashHosts) `
    | Set-Content "$deploymentDirectory\filebeat.yml" -force
}


function Uninstall
{
    iex 'net stop filebeat'
    iex "$PSScriptRoot\..\uninstall-service-filebeat.ps1"
}


function Install
{
    param([string]$deploymentDirectory)

    iex "$deploymentDirectory\install-service-filebeat.ps1"
    iex 'net start filebeat'

    if ($LASTEXITCODE -gt 0) 
    { 
      Write-Host 'Dailed to start filebeat' 
      exit 1 
    }
}

function CheckLogstash
{
    param([string]$config)

    $match = [regex]::Matches($config, '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
    Test-port $match 5044 10
}


function InstallFilebeat
{
    param([string]$env)

    # CheckLogstash $environments.$env.logstashHosts
    Uninstall
    CopyToinstallationDirectory $environments.$env.deploymentDirectory
    ReplaceConfig $env $environments.$env
    Install $environments.$env.deploymentDirectory
    
}